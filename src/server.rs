//    rps-tokio-server
//    Copyright (C) 2018  Christopher Vittal
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU Affero General Public License as published
//    by the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU Affero General Public License for more details.
//
//    You should have received a copy of the GNU Affero General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
extern crate futures;
extern crate tokio;

use std::io::BufReader;
use std::net::{IpAddr, Ipv6Addr, SocketAddr};
use std::sync::{Arc, Mutex};

use futures::sync::{mpsc, oneshot};

use tokio::io as tio;
use tokio::net::{TcpListener, TcpStream};
use tokio::prelude::*;

const PORT: u16 = 7531;

static MESSAGE: &'static str = "What do you throw, Rock (R), Paper (P), \
                                or Scissors (S)?\r\n";

fn main() {
    let addr = SocketAddr::new(IpAddr::V6(Ipv6Addr::from(0)), PORT);
    let listener = TcpListener::bind(&addr).expect("could not bind");

    let conns: Arc<Mutex<Option<TcpStream>>> = Arc::new(Mutex::new(None));

    let init = listener
        .incoming()
        .for_each(move |stream| {
            match stream.peer_addr() {
                Ok(a) => println!("got connection from {}", a),
                Err(e) => eprintln!(
                    "peer_addr: could not get peer's \
                     address - {}",
                    e
                ),
            }
            let conns2 = conns.clone();
            let welcome = tio::write_all(stream, MESSAGE)
                .and_then(move |(stream, _)| {
                    let c = conns2.clone();
                    let mut other = c.lock().expect("mutex poisoned");
                    if other.is_none() {
                        *other = Some(stream);
                    } else {
                        let other_stream = other.take().unwrap();
                        tokio::spawn(run_game(other_stream, stream));
                    }
                    Ok(())
                })
                .map_err(|e| {
                    eprintln!("write_all welcome: {}", e);
                });
            tokio::spawn(welcome);

            Ok(())
        })
        .map_err(|e| {
            eprintln!("incoming: {}", e);
        });

    tokio::run(init);
}

#[allow(unused)]
fn send_result(
    result: RpsResult,
    sender: oneshot::Sender<&'static str>,
) -> Result<(), &'static str> {
    use RpsResult::*;
    let msg = match result {
        Win => "You won! Congratulations!\r\n",
        Loss => "You lost. better luck next time.\r\n",
        Draw => "It's a draw! Try again soon.\r\n",
    };
    sender.send(msg)
}

fn run_game(conn_0: TcpStream, conn_1: TcpStream) -> impl Future<Item = (), Error = ()> {
    let (snd_tx, rcv_tx) = mpsc::channel::<(usize, Vec<u8>)>(0);
    let (p0_tmp, receiver_0) = oneshot::channel::<&'static str>();
    let (p1_tmp, receiver_1) = oneshot::channel::<&'static str>();

    let recv = rcv_tx.collect().and_then(move |mut v| {
        assert!(v.len() == 2);
        // unwraps are safe because we've asserted that the vector
        // has length 2
        let (tid_0, msg_0) = v.pop().unwrap();
        let (_, msg_1) = v.pop().unwrap();
        let throw_0 = msg_0.first().into();
        let throw_1 = msg_1.first().into();
        let (res_0, res_1) = RpsThrow::resolve(throw_0, throw_1);
        let (p0, p1) = if tid_0 == 0 {
            (p0_tmp, p1_tmp)
        } else {
            (p1_tmp, p0_tmp)
        };
        match (send_result(res_0, p0), send_result(res_1, p1)) {
            (Ok(_), Ok(_)) => Ok(()),
            _ => Err(()),
        }
    });

    recv.join3(
        run_conn(conn_0, 0, snd_tx.clone(), receiver_0),
        run_conn(conn_1, 1, snd_tx, receiver_1),
    ).map(|_| ())
}

#[allow(unused)]
fn run_conn(
    stream: TcpStream,
    tid: usize,
    sender: mpsc::Sender<(usize, Vec<u8>)>,
    receiver: oneshot::Receiver<&'static str>,
) -> impl Future<Item = (), Error = ()> {
    let s = BufReader::new(stream);
    let v = Vec::new();
    tio::read_until(s, b'\n', v)
        .and_then(move |(s, v)| {
            let snd = sender.send((tid, v)).then(|res| match res {
                Err(e) => {
                    eprintln!("mpsc send: {}", e);
                    Err(())
                }
                Ok(_) => Ok(()),
            });
            tokio::spawn(snd);
            Ok(s)
        })
        .map_err(|e| eprintln!("connection: {}", e))
        .join(receiver.map_err(|_| ()))
        .and_then(|(s, msg)| {
            tio::write_all(s.into_inner(), msg)
                .map_err(|e| eprintln!("write result: {}", e))
        })
        .then(|res| match res {
            Ok(_) => Ok(()),
            Err(_) => Err(()),
        })
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
enum RpsThrow {
    Rock,
    Paper,
    Scissors,
    Invalid,
}

enum RpsResult {
    Win,
    Loss,
    Draw,
}

impl From<u8> for RpsThrow {
    fn from(it: u8) -> Self {
        match it {
            b'r' | b'R' => RpsThrow::Rock,
            b'p' | b'P' => RpsThrow::Paper,
            b's' | b'S' => RpsThrow::Scissors,
            _ => RpsThrow::Invalid,
        }
    }
}

impl<'a> From<Option<&'a u8>> for RpsThrow {
    fn from(it: Option<&'a u8>) -> Self {
        it.map_or(RpsThrow::Invalid, |&x| x.into())
    }
}

impl RpsThrow {
    fn resolve(throw_0: Self, throw_1: Self) -> (RpsResult, RpsResult) {
        use RpsResult::*;
        use RpsThrow::*;
        match (throw_0, throw_1) {
            (Invalid, Invalid) => (Loss, Loss),
            (Invalid, _) => (Loss, Win),
            (_, Invalid) => (Win, Loss),
            (p, q) if p == q => (Draw, Draw),
            (Rock, Paper) | (Paper, Scissors) | (Scissors, Rock) => (Loss, Win),
            (Rock, Scissors) | (Scissors, Paper) | (Paper, Rock) => (Win, Loss),
            a => unreachable!("was {:?}", a),
        }
    }
}
